﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace Compare
{
    public partial class Form1 : Form
    {
        int notFind = 0;
        int pohFind = 0;

        public List<ListGen> FirstList = new List<ListGen>();
        public List<ListGen> TwousList = new List<ListGen>();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           

            string[] f = textBox1.Text.Split((char)13);
            string[] l = textBox2.Text.Split((char)13);
            bool onFind = false;

            foreach (string item in f)
            {
                string a = item.Trim();
                foreach (string item2 in l)
                {
                    string b = item2.Trim();
                    if (a == b)
                    {
                        onFind = true;
                        break;
                    }
                }

                //if (onFind == false) textBox3.Text = textBox3.Text + "\r\n" + a;
                onFind = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            int present = 0;

            string[] f = textBox2.Text.Split((char)13);
            string[] l = textBox1.Text.Split((char)13);
            bool onFind = false;

            foreach (string item in f)
            {
                string a = item.Trim();
                foreach (string item2 in l)
                {
                    string b = item2.Trim();
                    if (a == b)
                    {
                        onFind = true;
                        break;
                    }

                }

                if (onFind == false)
                {
                    foreach (string Line in l)
                    {
                        string[] lines = Line.Split(' ');
                        string[] startLine = a.Split(' ');
                    }
                }
               
                 

                //if (onFind == false) textBox3.Text = textBox3.Text + "\r\n" + a;
                onFind = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
            FirstList.RemoveRange(0, FirstList.Count);
            TwousList.RemoveRange(0, TwousList.Count);

            LoadLists();
            OnCheck();
            OnDeleteFinded();
            richTextBox1.AppendText("------Проблема 1 сравнения ------------ \r\n\r\n");
            OnDetail(ref FirstList,ref TwousList);
            OnDeleteFinded();
            richTextBox1.AppendText("------Проблема 2 сравнения ------------ \r\n\r\n");
            OnDetail(ref TwousList, ref FirstList);

            notFind = 0;
            pohFind = 0;
        }

        private void LoadLists()
        {
            string[] first =  textBox1.Text.Split((char)13);
            string[] second = textBox2.Text.Split((char)13);

            foreach (string item1 in first) FirstList.Add(new ListGen(item1.Trim()));
            foreach (string item2 in second)TwousList.Add(new ListGen(item2.Trim()));

            label1.Text = string.Format("Загружено 1 - {0} шт.", FirstList.Count);
            label2.Text = string.Format("Загружено 2 - {0} шт.", TwousList.Count);
        }

        private void OnCheck()
        {
            foreach (ListGen item1 in FirstList)
            {
                foreach (ListGen item2 in TwousList)
                {
                    if (item1.Compare(item2))
                    {
                        item1.notFinded = false;
                        item2.notFinded = false;
                    } 
                }
            }
        }

        private void OnDeleteFinded()
        {
            int i = 0;
            while (i != FirstList.Count)
            {
                if (FirstList[i].notFinded == false) FirstList.RemoveRange(i, 1);
                else i++;
            }

            i = 0;

            while (i != TwousList.Count)
            {
                if (TwousList[i].notFinded == false) TwousList.RemoveRange(i, 1);
                else i++;
            }
        }

        private void OnDetail(ref List<ListGen> OneArg, ref List<ListGen> TwoArg)
        {
            

            foreach (ListGen item1 in OneArg)
            {
                int sovp = 0;
                bool redText = true;
                bool firstPrint = true;

                foreach (ListGen item2 in TwoArg)
                {

                    sovp = CompareString(item1.fName, item2.fName);

                    if (sovp != 0)
                    {
                        redText = false;

                        if (firstPrint) richTextBox1.AppendText(string.Format("{0} \r\n -->{1} ( {2} )", item1.fName, item2.fName, sovp), Color.Yellow, Color.Black);
                            else richTextBox1.AppendText(string.Format("\r\n -->{0} ( {1} )", item2.fName, sovp), Color.Yellow, Color.Black);
                        firstPrint = false;

                        if (sovp >= 2)
                        {
                            if (!checkBox2.Checked)
                            {
                                pohFind++;
                                item1.notFinded = false;
                                item2.notFinded = false;
                            }
                        }

                        
                    }
                }



                if (redText)
                {
                    richTextBox1.AppendText(string.Format("{0}", item1.fName), Color.Red, Color.White);
                    notFind++;
                }

                richTextBox1.AppendText("\r\n\r\n");
                
            }

            if (notFind != 0)
            {
                label3.Text = string.Format("Не найдено - {0} шт.", notFind);
                label4.Text = string.Format("Похожие - {0} шт.", pohFind);
            }

        }
   

        private int CompareString(string Text1, string Text2)
        {
            string[] Detail1 = Text1.Split(' ');
            string[] Detail2 = Text2.Split(' ');

            int Sovp = 0;

            foreach (string CuteString1 in Detail1)
            {
                foreach (string CuteString2 in Detail2)
                {
                    if (CuteString1 == CuteString2)
                    {
                        Sovp += 1;
                        break;
                    }
                }
            }

            return Sovp;
        }

    }


    public class ListGen  
	{
		public string fName;
        public bool notFinded;

        public ListGen(string Name)
        {
            fName = Name;
            notFinded = true;
        }

        public bool Compare(ListGen lg)
        {
            if (fName == lg.fName) return true;
            return false;
            
        }

        
    
	}   
    

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color,Color Fcolor)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;
            box.SelectionBackColor = color;
            box.SelectionColor = Fcolor;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
